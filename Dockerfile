FROM node:12

ARG APP_DIR=/home/node/app

WORKDIR ${APP_DIR}

RUN mkdir -p node_modules && chown -R node:node ${APP_DIR}

COPY package.json ${APP_DIR}/package.json

RUN npm i -g @angular/cli && \
    npm install

COPY . ${APP_DIR}
