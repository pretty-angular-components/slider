# Angular2 Carousel

This project for Angular 2+ (currently supported version 8+)  

| Library version | npm tag | Angular version |
|-----------------|:-------:|:---------------:|
| 6.x             |  ng13   |   Angular 13    |
| 5.x             |  ng12   |   Angular 12    |
| 4.x             |  ng11   |   Angular 11    |
| 3.x             |  ng10   |   Angular 10    |
| 2.x             |   ng9   |    Angular 9    |
| 1.2.x           |   ng8   |    Angular 8    |

Source code of project with examples are available by this link: 
[gitlab.com/pretty-angular-components/slider](https://gitlab.com/pretty-angular-components/slider) 

Site with demos available here: [prettyangular.kinect.pro](https://prettyangular.kinect.pro)

This project has based on several requirements: 
was required slider which would support html components with text (links), 
events bindings and which can slide up to right corner of last element.
So it is main features, available by default.

**Note 1:** If you develop for IE / Edge browser, do not forget include all required polyfills, 
such as `classlist.js` and `web-animations-js`

**Note 2:** Please note, if you use images, you should assign the width or wrap them in 'div' tag with assigned width property.
            It required because resize sensor can't detect changing of size of image on load event.
            Alternatively - you need to manually run method 'reset()' on each 'onLoad' image event.
        
## Features
#### Adding your own template
To create a carousel from elements, you must pass your template to the component.
Elements can be either interconnected into a collection 
(for example, an array of users, an array of links to photos with descriptions), 
or independent (for example, a photo with a description + user data + product card). 
You are not limited in the choice of elements to display.

#### Using elements of different width
You can use elements with different width. 
Scrolling such items will work correctly for each item.

#### Infinite mod
You can use loop scrolling if you want the animation to last in a circle.

#### Custom styles
You can customize the scroll controllers to your liking, or disable them.

#### Custom animation
You can customize the duration, delay and animation type.

#### Auto animation
You can enable the auto-scrolling and pass the number of milliseconds that must 
elapse before the animation.
Animation will be carried out in an automatic mode until it reaches the last element, 
or the user focuses on the carousel.

## Installation

### How to install
#### With npm
```npm install @kinect-pro/ngp-carousel```   

#### With yarn 
```yarn add @kinect-pro/ngp-carousel```   

To install target version you can use tags `ng8`, `ng9`, `ng10` etc:  
```npm install @kinect-pro/ngp-carousel@ng9```

### How setup 
#### Add NgpCarouselModule and BrowserAnimationsModule module
##### app.module
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgpCarouselModule } from '@kinect-pro/ngp-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgpCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

#### Add carousel component to template
##### app.component.html
```
<ngp-carousel>
  <ng-template *ngFor="let elem of elements" ngpCarouselItem>
    <div class="your-classes">
      <span>{{elem.name}}</span>
    </div>
  </ng-template>
</ngp-carousel>
```

## Usage examples
##### app.component.ts
```
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  elements = [
    {name: 'Jenfirebreatha'}, {name: 'Brainsra'}, {name: 'Pacoffin'}, {name: 'Quatorifiliny'},
    {name: 'Rosepop'}, {name: 'Agamason'}, {name: 'Mooreconda'}, {name: 'Hayetwinkle'}
  ];

  images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg'];
}
```

##### app.component.html

**1 Default carousel** 
```
<ngp-carousel>
  <ng-template *ngFor="let elem of elements" ngpCarouselItem>
    <div class="elem-item border border-dark text-center">
      <span>{{elem.name}}</span>
    </div>
  </ng-template>
</ngp-carousel>
```

**2. Custom buttons**  

You can add your own styled buttons by using `ngpCarouselButton` directive 
and property `slot` (allowed values `left` or `right`) 
that decide on what place set your control.  

```
<ngp-carousel>
  <ng-template *ngFor="let elem of elements" ngpCarouselItem>
    <div class="elem-item border border-dark text-center">
      <span>{{elem.name}}</span>
    </div>
  </ng-template>
  <div style="padding: 5px" ngpCarouselButton slot="left">
    <button class="btn btn-primary">Left</button>
  </div>
  <div style="padding: 5px" ngpCarouselButton slot="right">
    <button class="btn btn-primary">Right</button>
  </div>
</ngp-carousel>
```

Carousel by default will add click event on your component. 
But if you wish avoid this behaviours, you cant set `disableDefaultClick` flag to `false`
```
<button class="btn btn-lg btn-link" ngpCarouselButton [disableDefaultClick]="true" slot="right" (click)="rightClick()">
  <fa-icon [icon]="faRight" [size]="'2x'"></fa-icon>
</button>
```

**3. Full width items**  
By default carousel use own element width for display them.
But sometimes needed to use fully carousel width items.
To achieve that you can use flag `perPage` set to `true`
```
<ngp-carousel [perPage]="true"
            [innerButtons]="true"
            [loop]="true"
            [animationDelay]="100"
            [animationDuration]="500"
            [animationEasing]="'ease-in-out'"
>
  <ng-template *ngFor="let img of images" ngpCarouselItem>
    <img src="assets/images/{{img}}" class="img-thumbnail">
  </ng-template>
</ngp-carousel>
```

##### More examples you can find on official homepage of project.  

## Properties
<table>
    <tr>
        <th>Property</th>
        <th>Type</th>
        <th>Default</th>        
        <th>Description</th>
    </tr>    
    <tr>
        <td>index</td><td>number</td><td>0</td>
        <td>Only for reading. Returns index of current template item that carousel cursor is pointed.</td>        
    </tr>
    <tr>
        <td>isOnStart</td><td>boolean</td><td>true</td>
        <td>Only for reading. returns `true` when carousel cursor positioned on first of templates.</td>        
    </tr>
    <tr>
        <td>isOnEnd</td><td>boolean</td><td>false</td>
        <td>Only for reading. returns `true` when carousel cursor positioned on last of templates.</td>        
    </tr>
    <tr>
        <td>disabled</td><td>boolean</td><td>false</td>
        <td>disable carousel activity.</td>
    </tr>
    <tr>
        <td>loop</td><td>boolean</td><td>false</td>
        <td>loop scrolling, the animation will to last in a circle.</td>
    </tr>
    <tr>
        <td>rtl</td><td>boolean</td><td>false</td>
        <td>right to lest carousel items orientation.</td>
    </tr>
    <tr>
        <td>fullSizeItems</td><td>boolean</td><td>false</td>
        <td>
            Makes items sized on full width of carousel container.
            *has removed since of version 2.1.0
        </td>
    </tr>
    <tr>
        <td>perPage</td><td>boolean | number</td><td>false</td>
        <td>
            Makes items seized on full or partial width of carousel container.
            *has added in version 2.1.0
        </td>
    </tr>
    <tr>
        <td>buttonsMode</td><td>string | enControlsMode</td><td>'show'</td>
        <td>Button display mode. Possible values:<br> 
            'show' - always display<br>
            'hide' - always hidden<br> 
            'auto' - will display buttons if width of content 
            smaller than width of carouses   
        </td>
    </tr>
    <tr>
        <td>innerButtons</td><td>boolean</td><td>false</td>
        <td>Flag allow to switch position where to place buttons, 
        on the sides of carousel contend or on top of.</td>
    </tr>
    <tr>
        <td>autoSlide</td><td>number | boolean</td><td>false</td>
        <td>Time to automatic slide items in ms.</td>
    </tr>
    <tr>
        <td>autoSlideDirection</td><td>string | enDirection </td><td>'next'</td>
        <td>Direction of automatic slide items. Possible values:<br>
            'next' - slide to next item<br>
            'prev' - slide to previous item
        </td>
    </tr>
    <tr>
        <td>animationDelay</td><td>number</td><td>0</td><td></td>
    </tr>
    <tr>
        <td>animationDuration</td><td>number</td><td>250</td><td></td>
    </tr>
    <tr>
        <td>animationEasing</td><td>string</td><td>'ease-in'</td><td></td>
    </tr>
</table>

## Methods

You can manipulate carousel manually by using `@ViewChild()`  
Methods:  
**fill()** - manual fill carousel by content items
**clear()** - remove all carousel items
**prev()** - move items to previous     
**next()** - move items to next   
**toStart()** - slide to first. working only when `loop` mode disabled  
**toEnd()** -  slide to last. working only when `loop` mode disabled

## Events
You can subscribe on two event emitters:   
**animStart**:  - emit boolean value when animation is started  
**animFinish**: - emit boolean value when animation is finished  

