import { TestBed } from '@angular/core/testing';

import { NgpCarouselService } from './ngp-carousel.service';

describe('NgpCarouselService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgpCarouselService = TestBed.get(NgpCarouselService);
    expect(service).toBeTruthy();
  });
});
