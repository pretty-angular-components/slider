import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgpCarouselComponent } from './ngp-carousel.component';

describe('Angular2CarouselComponent', () => {
  let component: NgpCarouselComponent;
  let fixture: ComponentFixture<NgpCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgpCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgpCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
