import {Directive, ElementRef, Input} from '@angular/core';
import {enSlot} from '../const';

@Directive({
  selector: '[ngpCarouselButton]'
})
export class CarouselButtonDirective {
  @Input() disableDefaultClick = false;
  @Input() slot: enSlot | string = null;

  constructor(public el: ElementRef) { }
}
