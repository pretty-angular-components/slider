import {ElementRef} from '@angular/core';

export class WidthChangedEvent {
  constructor(
    readonly element: ElementRef,
    readonly width: number,
    readonly oldWidth: number
  ) {}
}
