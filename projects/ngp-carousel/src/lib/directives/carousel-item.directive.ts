import {Directive, ElementRef, Input, TemplateRef} from '@angular/core';

@Directive({
  selector: '[ngpCarouselItem]'
})
export class CarouselItemDirective {
  @Input() fullSize = false;

  constructor(public templateRef: TemplateRef<any>, public el: ElementRef) { }
}
