import {Directive, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {ResizeSensor} from 'css-element-queries';
import {WidthChangedEvent} from './width-changed-event';

@Directive({
  selector: '[ngpWidthChanged]'
})
export class WidthChangedDirective implements OnInit {

  @Output() readonly ngpWidthChanged = new EventEmitter<WidthChangedEvent>();

  private oldWidth: number;
  private resizeSensor;

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    this.resizeSensor = new ResizeSensor(this.el.nativeElement, () => this.onResized());
    this.onResized();
  }

  private onResized() {
    const width = this.el.nativeElement.clientWidth;

    if (width === this.oldWidth) {
      return;
    }

    const event = new WidthChangedEvent(this.el, width, this.oldWidth);
    this.oldWidth = this.el.nativeElement.clientWidth;

    this.ngpWidthChanged.emit(event);
  }

}
