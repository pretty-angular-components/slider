import {Component, ElementRef, Input} from '@angular/core';
import {enDirection, enSlot} from '../const';

@Component({
  selector: 'ngp-carousel-button',
  templateUrl: './carousel-button.component.html',
  styleUrls: ['./carousel-button.component.scss']
})
export class CarouselButtonComponent {

  @Input() direction: enDirection;
  @Input() position: enSlot;
  @Input() disabled: boolean;

  constructor(public el: ElementRef) { }
}
