export enum enControlsMode {
  auto = 'auto',
  hide = 'hide',
  show = 'show'
}

export enum enSlot {
  left = 'left',
  right = 'right',
}

export enum enDirection {
  prev = 'prev',
  next = 'next',
}
