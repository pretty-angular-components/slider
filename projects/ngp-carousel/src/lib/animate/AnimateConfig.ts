import {Smooth} from './config';

export interface AnimateOptions {
  delayVal ?: string | number;
  durationVal ?: string | number;
  easingVal ?: Smooth | string;
}

export class AnimateConfig implements AnimateOptions {
  delayVal: string | number = 0;
  durationVal: string | number = '250ms';
  easingVal: Smooth | string = Smooth.easeIn;

  constructor(animation: AnimateOptions = {delayVal: 0, durationVal: 250, easingVal: Smooth.easeIn}) {
    this.delay = animation.delayVal;
    this.duration = animation.durationVal;
    this.easing = animation.easingVal;
  }

  set delay(value) {
    this.delayVal = AnimateConfig.convertTiming(value);
  }

  set duration(value) {
    this.durationVal = AnimateConfig.convertTiming(value);
  }

  set easing(value) {
    this.easingVal = value;
  }

  get timing() {
      return `${this.durationVal} ${this.delayVal ? this.delayVal : ''} ${this.easingVal}`;
  }

  static convertTiming(timing: string | number): string {
    if (typeof timing === 'number') {
      return timing + 'ms';
    }

    const timingRegexp = /^(\d{1,4}(ms|s)|(\d{1,2}(.\d{1,2})?s))\b/;
    if (timingRegexp.test(timing)) {
      return timing;
    }
  }
}
