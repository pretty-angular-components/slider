import {enSlot} from './const';
import {CarouselButtonDirective} from './directives/carousel-button.directive';
import {CarouselButtonComponent} from './carousel-button/carousel-button.component';
import {ComponentFactory, ComponentRef, ElementRef, ViewContainerRef} from '@angular/core';

export class Slot {
  outerButton: CarouselButtonDirective;
  innerButton: ComponentRef<CarouselButtonComponent>;
  slotRef: ViewContainerRef;

  set disabled(val: boolean) {
    if (this.innerButton) {
      this.innerButton.instance.disabled = val;
    }
  }

  constructor(public position: enSlot) {}

  validate(factory: ComponentFactory<CarouselButtonComponent>) {
    if (this.outerButton || this.innerButton) {
      return;
    }
    this.innerButton = this.slotRef.createComponent(factory);
    this.innerButton.instance.position = this.position;
  }

  get nativeElementRef(): ElementRef | null {
    return this.outerButton ? this.outerButton.el.nativeElement : (this.innerButton ? this.innerButton.instance.el.nativeElement : null);
  }

  get isDisableDefaultClick(): boolean {
    return this.outerButton ? this.outerButton.disableDefaultClick : false;
  }
}
