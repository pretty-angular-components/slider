/*
 * Public API Surface of ngp-carousel
 */

export * from './lib/ngp-carousel.service';
export * from './lib/ngp-carousel.component';
export * from './lib/directives/carousel-item.directive';
export * from './lib/directives/carousel-button.directive';
export * from './lib/carousel-button/carousel-button.component';
export * from './lib/ngp-carousel.module';
export * from './lib/const';
export * from './lib/directives/width-changed-event';
export * from './lib/directives/width-changed.directive';
