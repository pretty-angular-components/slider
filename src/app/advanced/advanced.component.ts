import {AfterViewInit, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {elements} from '../elements';
import {NgpCarouselComponent} from '../../../projects/ngp-carousel/src/lib/ngp-carousel.component';
import {enControlsMode} from '../../../projects/ngp-carousel/src/lib/const';


@Component({
  selector: 'app-advanced',
  templateUrl: './advanced.component.html',
  styleUrls: ['./advanced.component.scss']
})
export class AdvancedComponent implements AfterViewInit {

  @ViewChild('example5', {static: true}) example5Carousel: NgpCarouselComponent;
  @ViewChild('example5rtl', {static: true}) example5RtlCarousel: NgpCarouselComponent;
  @ViewChild('example6', {static: true}) example6Carousel: NgpCarouselComponent;
  @ViewChild('example9', {static: true}) example9Carousel: NgpCarouselComponent;

  enControlsMode = enControlsMode;
  elements = elements;
  example6Index = 0;

  loop10 = true;
  enabled10 = true;
  innerButtons10 = false;

  constructor(private cd: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.example6Carousel.animStart.subscribe(() => this.example6Index = this.example6Carousel.index);
    this.cd.detectChanges();
  }

  toStart() {
    this.example5Carousel.toStart();
  }

  toEnd() {
    this.example5Carousel.toEnd();
  }

  toStartRtl() {
    this.example5RtlCarousel.toStart();
  }

  toEndRtl() {
    this.example5RtlCarousel.toEnd();
  }

  log(text) {
    console.log(text);
  }

  removeAllFrom9() {
    this.example9Carousel.clear();
  }

  fillAllTo9() {
    this.example9Carousel.fill();
  }
}
