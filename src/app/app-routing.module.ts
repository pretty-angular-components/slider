import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BaseComponent} from './base/base.component';
import {AdvancedComponent} from './advanced/advanced.component';
import {ImagesComponent} from './images/images.component';
import {BootstrapComponent} from './bootsrap/bootstrap.component';

const routes: Routes = [
  { path: '', component: BaseComponent},
  { path: 'advanced', component: AdvancedComponent},
  { path: 'images', component: ImagesComponent},
  { path: 'bootstrap', component: BootstrapComponent},
  {
    path: 'unified-width',
    loadChildren: () => import('./unified-width/unified-width.module').then(m => m.UnifiedWidthModule)
  },

  {
    path: 'content-manipulation',
    loadChildren: () => import('./content-manipulation/content-manipulation.module')
      .then(m => m.ContentManipulationModule)
  },

  { path: '**', redirectTo: '' }
];

@Injectable()
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
