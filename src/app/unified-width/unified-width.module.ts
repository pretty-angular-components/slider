import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnifiedWidthComponent } from './unified-width.component';
import {UnifiedWidthRoutingModule} from './unified-width-routing.module';
import {NgpCarouselModule} from '../../../projects/ngp-carousel/src/lib/ngp-carousel.module';

@NgModule({
  declarations: [UnifiedWidthComponent],
  imports: [
    CommonModule,
    UnifiedWidthRoutingModule,
    NgpCarouselModule,
  ]
})
export class UnifiedWidthModule { }
