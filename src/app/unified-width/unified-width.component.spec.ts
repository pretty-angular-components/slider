import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnifiedWidthComponent } from './unified-width.component';

describe('UnifiedWidthComponent', () => {
  let component: UnifiedWidthComponent;
  let fixture: ComponentFixture<UnifiedWidthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnifiedWidthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnifiedWidthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
