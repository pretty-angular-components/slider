import {Component} from '@angular/core';
import {elements} from '../elements';

@Component({
  selector: 'app-unified-width',
  templateUrl: './unified-width.component.html',
  styleUrls: ['./unified-width.component.scss']
})
export class UnifiedWidthComponent {

  elements = elements;
  numPerPage: number | boolean = false;

  constructor() {
  }
}
