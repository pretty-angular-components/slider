import { Component } from '@angular/core';
import {elements} from '../elements';

@Component({
  selector: 'app-content-manipulation',
  templateUrl: './content-manipulation.component.html',
  styleUrls: ['./content-manipulation.component.scss']
})
export class ContentManipulationComponent {

  elements = [...elements];
  show = true;

  constructor() { }

  append() {
    this.elements.push({name: 'new item' + (this.elements.length + 1), img: null});
  }

  remove() {
    this.elements.pop();
  }

  prepend() {
    this.elements.unshift({name: 'new item' + (this.elements.length + 1), img: null});
  }

  shift() {
    this.elements.shift();
  }

}
