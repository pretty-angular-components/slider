import {NgModule, Injectable} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentManipulationComponent} from './content-manipulation.component';

const routes: Routes = [
  {path: '', component: ContentManipulationComponent}
];

@Injectable()
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentManipulationRoutingModule {
}
