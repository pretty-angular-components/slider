import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentManipulationComponent } from './content-manipulation.component';

describe('UnifiedWidthComponent', () => {
  let component: ContentManipulationComponent;
  let fixture: ComponentFixture<ContentManipulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentManipulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentManipulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
