import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContentManipulationComponent} from './content-manipulation.component';
import {ContentManipulationRoutingModule} from './content-manipulation-routing.module';
import {NgpCarouselModule} from '../../../projects/ngp-carousel/src/lib/ngp-carousel.module';

@NgModule({
  declarations: [ContentManipulationComponent],
  imports: [
    CommonModule,
    ContentManipulationRoutingModule,
    NgpCarouselModule,
  ]
})
export class ContentManipulationModule {
}
