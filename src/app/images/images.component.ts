import {Component, ViewChild} from '@angular/core';
import {NgpCarouselComponent} from '../../../projects/ngp-carousel/src/lib/ngp-carousel.component';
import {faArrowLeft, faArrowRight} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent {

  @ViewChild('example12', {static: true}) example12Carousel: NgpCarouselComponent;
  @ViewChild('example9', {static: true}) example9Carousel: NgpCarouselComponent;

  images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg'];
  faLeft = faArrowLeft;
  faRight = faArrowRight;

  constructor() { }

  leftClick() {
    this.example12Carousel.prev();
  }

  rightClick() {
    this.example12Carousel.next();
  }

  test() {

  }

  widthUpdated() {
    this.example9Carousel.reset();
  }
}
