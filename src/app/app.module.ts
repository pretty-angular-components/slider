import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppComponent} from './app.component';
import {NgpCarouselModule} from '../../projects/ngp-carousel/src/lib/ngp-carousel.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AppRoutingModule} from './app-routing.module';
import {BaseComponent} from './base/base.component';
import {AdvancedComponent} from './advanced/advanced.component';
import {ImagesComponent} from './images/images.component';
import {BootstrapComponent} from './bootsrap/bootstrap.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    AdvancedComponent,
    ImagesComponent,
    BootstrapComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgpCarouselModule,
    FontAwesomeModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
