import { Component } from '@angular/core';
import {elements} from '../elements';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent  {

  constructor() { }

  elements = elements;
}
