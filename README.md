# Angular Carousel project

This project for Angular 2+ (currently supported version 8)  

Source code of project with examples are available by this link: 
[gitlab.com/pretty-angular-components/slider](https://gitlab.com/pretty-angular-components/carousel) 

Site with demos available here: [prettyangular.kinect.pro](https://prettyangular.kinect.pro)

This project has based on several requirements: 
was required slider which would support html components with text (links), 
events bindings and which can slide up to right corner of last element.
So it is main features, available by default.
